package cn.li.projectli.pojo;

import java.util.Date;

/**
 * @Author 秋殇
 * @Date 2024/1/31 16:04
 * @Version 1.0
 */
public class Cinvoicedetailstable {

    private Integer id;
    private String invoicenumber;
    private String buyername;
    private String buyertaxid;
    private String sellername;
    private String sellertaxid;
    private String buyerbank;
    private String buyeraccount;
    private String sellerbank;
    private String selleraccount;
    private String totalamount;
    private String totaltax;
    private String invoicedate;
    private String invoicetype;
    private Date createtime;
    private Date modifytime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvoicenumber() {
        return invoicenumber;
    }

    public void setInvoicenumber(String invoicenumber) {
        this.invoicenumber = invoicenumber;
    }

    public String getBuyername() {
        return buyername;
    }

    public void setBuyername(String buyername) {
        this.buyername = buyername;
    }

    public String getBuyertaxid() {
        return buyertaxid;
    }

    public void setBuyertaxid(String buyertaxid) {
        this.buyertaxid = buyertaxid;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getSellertaxid() {
        return sellertaxid;
    }

    public void setSellertaxid(String sellertaxid) {
        this.sellertaxid = sellertaxid;
    }

    public String getBuyerbank() {
        return buyerbank;
    }

    public void setBuyerbank(String buyerbank) {
        this.buyerbank = buyerbank;
    }

    public String getBuyeraccount() {
        return buyeraccount;
    }

    public void setBuyeraccount(String buyeraccount) {
        this.buyeraccount = buyeraccount;
    }

    public String getSellerbank() {
        return sellerbank;
    }

    public void setSellerbank(String sellerbank) {
        this.sellerbank = sellerbank;
    }

    public String getSelleraccount() {
        return selleraccount;
    }

    public void setSelleraccount(String selleraccount) {
        this.selleraccount = selleraccount;
    }

    public String getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount;
    }

    public String getTotaltax() {
        return totaltax;
    }

    public void setTotaltax(String totaltax) {
        this.totaltax = totaltax;
    }

    public String getInvoicedate() {
        return invoicedate;
    }

    public void setInvoicedate(String invoicedate) {
        this.invoicedate = invoicedate;
    }

    public String getInvoicetype() {
        return invoicetype;
    }

    public void setInvoicetype(String invoicetype) {
        this.invoicetype = invoicetype;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }
}