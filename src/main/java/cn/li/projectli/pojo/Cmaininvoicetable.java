package cn.li.projectli.pojo;

import java.util.Date;

/**
 * @Author 秋殇
 * @Date 2024/1/31 16:00
 * @Version 1.0
 */
public class Cmaininvoicetable {

    private Integer invoiceid;
    private String invoicenumber;
    private String invoicedate;
    private String pricetaxtotal;
    private Date createtime;
    private Date updatetime;
    private String filelocation;

    public Integer getInvoiceid() {
        return invoiceid;
    }

    public void setInvoiceid(Integer invoiceid) {
        this.invoiceid = invoiceid;
    }

    public String getInvoicenumber() {
        return invoicenumber;
    }

    public void setInvoicenumber(String invoicenumber) {
        this.invoicenumber = invoicenumber.trim();
    }

    public String getInvoicedate() {
        return invoicedate;
    }

    public void setInvoicedate(String invoicedate) {
        this.invoicedate = invoicedate.trim();
    }

    public String getPricetaxtotal() {
        return pricetaxtotal;
    }

    public void setPricetaxtotal(String pricetaxtotal) {
        this.pricetaxtotal = pricetaxtotal.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public String getFilelocation() {
        return filelocation;
    }

    public void setFilelocation(String filelocation) {
        this.filelocation = filelocation.trim();
    }
}
