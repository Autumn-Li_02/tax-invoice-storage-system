package cn.li.projectli.pojo;

import java.util.Date;

/**
 * @Author 秋殇
 * @Date 2024/1/31 16:05
 * @Version 1.0
 */
public class Cinvoicecontenttable {

    private Integer id;
    private String invoicenumber;
    private String itemname;
    private String specification;
    private String unit;
    private String quantity;
    private String unitprice;
    private String amount;
    private String taxrate;
    private String taxamount;
    private String totalamount;
    private String totaltaxamount;
    private Date createtime;
    private Date modifytime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvoicenumber() {
        return invoicenumber.trim();
    }

    public void setInvoicenumber(String invoicenumber) {
        this.invoicenumber = invoicenumber == null ? null : invoicenumber.trim();
    }

    public String getItemname() {
        return itemname.trim();
    }

    public void setItemname(String itemname) {
        this.itemname = itemname == null ? null : itemname.trim();
    }

    public String getSpecification() {
        return specification.trim();
    }

    public void setSpecification(String specification) {
        this.specification = specification == null ? null : specification.trim();
    }

    public String getUnit() {
        return unit.trim();
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }

    public String getQuantity() {
        return quantity.trim();
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity == null ? null : quantity.trim();
    }

    public String getUnitprice() {
        return unitprice.trim();
    }

    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice == null ? null : unitprice.trim();
    }

    public String getAmount() {
        return amount.trim();
    }

    public void setAmount(String amount) {
        this.amount = amount == null ? null : amount.trim();
    }

    public String getTaxrate() {
        return taxrate.trim();
    }

    public void setTaxrate(String taxrate) {
        this.taxrate = taxrate == null ? null : taxrate.trim();
    }

    public String getTaxamount() {
        return taxamount.trim();
    }

    public void setTaxamount(String taxamount) {
        this.taxamount = taxamount == null ? null : taxamount.trim();
    }

    public String getTotalamount() {
        return totalamount.trim();
    }

    public void setTotalamount(String totalamount) {
        this.totalamount = totalamount == null ? null : totalamount.trim();
    }

    public String getTotaltaxamount() {
        return totaltaxamount.trim();
    }

    public void setTotaltaxamount(String totaltaxamount) {
        this.totaltaxamount = totaltaxamount == null ? null : totaltaxamount.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Date getModifytime() {
        return modifytime;
    }

    public void setModifytime(Date modifytime) {
        this.modifytime = modifytime;
    }
}
