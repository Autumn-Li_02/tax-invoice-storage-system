package cn.li.projectli.mappers;

import cn.li.projectli.pojo.Cinvoicedetailstable;

/**
 * @Author 秋殇
 * @Date 2024/1/31 16:07
 * @Version 1.0
 */
public interface CinvoicedetailstableMapper {

    /**
     * 根据主键ID删除表中的记录
     *
     * @param id 主键ID
     * @return 删除的记录数
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入一个新的记录到表中
     *
     * @param record 要插入的记录
     * @return 插入的记录数
     */
    int insert(Cinvoicedetailstable record);

    /**
     * 插入一个新的记录到表中，只插入非空字段的值
     *
     * @param record 要插入的记录
     * @return 插入的记录数
     */
    int insertSelective(Cinvoicedetailstable record);

    /**
     * 根据主键ID查询并返回表中的一条记录
     *
     * @param id 主键ID
     * @return 查询到的记录
     */
    Cinvoicedetailstable selectByPrimaryKey(Integer id);

    /**
     * 根据主键更新表中的记录，只有非空字段会被更新
     *
     * @param record 要更新的记录
     * @return 更新的记录数
     */
    int updateByPrimaryKeySelective(Cinvoicedetailstable record);

    /**
     * 根据主键完全更新表中的记录，所有字段都会被更新
     *
     * @param record 要更新的记录
     * @return 更新的记录数
     */
    int updateByPrimaryKey(Cinvoicedetailstable record);
}
