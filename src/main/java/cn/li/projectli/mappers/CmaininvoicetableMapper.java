package cn.li.projectli.mappers;

import cn.li.projectli.pojo.Cmaininvoicetable;

/**
 * @Author 秋殇
 * @Date 2024/1/31 16:08
 * @Version 1.0
 */
public interface CmaininvoicetableMapper {

    /**
     * 根据主键invoiceid删除cmaininvoicetable表中的记录
     *
     * @param invoiceid 主键ID
     * @return 删除的记录数
     */
    int deleteByPrimaryKey(Integer invoiceid);

    /**
     * 插入一个新的记录到cmaininvoicetable表中
     *
     * @param record 要插入的记录
     * @return 插入的记录数
     */
    int insert(Cmaininvoicetable record);

    /**
     * 选择性地插入一个新的记录到cmaininvoicetable表中，只插入非空字段的值
     *
     * @param record 要插入的记录
     * @return 插入的记录数
     */
    int insertSelective(Cmaininvoicetable record);

    /**
     * 根据主键invoiceid查询并返回cmaininvoicetable表中的一条记录
     *
     * @param invoiceid 主键ID
     * @return 查询到的记录
     */
    Cmaininvoicetable selectByPrimaryKey(Integer invoiceid);

    /**
     * 根据主键更新cmaininvoicetable表中的记录（包括BLOB类型字段），只有非空字段会被更新
     *
     * @param record 要更新的记录
     * @return 更新的记录数
     */
    int updateByPrimaryKeySelective(Cmaininvoicetable record);

    /**
     * 根据主键完全更新cmaininvoicetable表中的记录，包括BLOB类型字段在内的所有字段都会被更新
     *
     * @param record 要更新的记录
     * @return 更新的记录数
     */
    int updateByPrimaryKeyWithBLOBs(Cmaininvoicetable record);

    /**
     * 根据主键更新cmaininvoicetable表中的记录，不包括BLOB类型字段，只有非空字段会被更新
     *
     * @param record 要更新的记录
     * @return 更新的记录数
     */
    int updateByPrimaryKey(Cmaininvoicetable record);
}
