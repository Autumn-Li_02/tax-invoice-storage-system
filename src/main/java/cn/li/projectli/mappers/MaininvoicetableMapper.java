package cn.li.projectli.mappers;

import cn.li.projectli.pojo.Maininvoicetable;

/**
 * @Author 秋殇
 * @Date 2024/1/31 16:09
 * @Version 1.0
 */
public interface MaininvoicetableMapper {

    /**
     * 根据主键删除记录
     *
     * @param invoiceid 主键
     * @return 删除记录的数量
     */
    int deleteByPrimaryKey(Integer invoiceid);

    /**
     * 插入记录
     *
     * @param record 记录
     * @return 插入记录的数量
     */
    int insert(Maininvoicetable record);

    /**
     * 选择记录
     *
     * @param invoiceid 主键
     * @return 选择的记录
     */
    Maininvoicetable selectByPrimaryKey(Integer invoiceid);

    /**
     * 根据主键更新记录
     *
     * @param record 记录
     * @return 更新记录的数量
     */
    int updateByPrimaryKeySelective(Maininvoicetable record);

    /**
     * 根据主键更新记录（包含BLOB字段）
     *
     * @param record 记录
     * @return 更新记录的数量
     */
    int updateByPrimaryKeyWithBLOBs(Maininvoicetable record);

    /**
     * 根据主键更新记录
     *
     * @param record 记录
     * @return 更新记录的数量
     */
    int updateByPrimaryKey(Maininvoicetable record);

    /**
     * 根据传入的 {@link Maininvoicetable} 对象的部分属性有选择地插入记录
     *
     * @param record 需要插入的部分属性非空的 {@link Maininvoicetable} 对象
     * @return 插入记录的数量
     */
    int insertSelective(Maininvoicetable record);
}
