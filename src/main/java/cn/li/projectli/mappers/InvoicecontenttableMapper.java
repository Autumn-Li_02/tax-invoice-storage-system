package cn.li.projectli.mappers;

import cn.li.projectli.pojo.Invoicecontenttable;

public interface InvoicecontenttableMapper {

    /**
     * 根据主键ID删除invoicecontenttable表中的记录
     *
     * @param id 主键ID
     * @return 删除的记录数
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 插入一个新的记录到invoicecontenttable表中
     *
     * @param record 要插入的记录
     * @return 插入的记录数
     */
    int insert(Invoicecontenttable record);

    /**
     * 选择性地插入一个新的记录到invoicecontenttable表中，只插入非空字段的值
     *
     * @param record 要插入的记录
     * @return 插入的记录数
     */
    int insertSelective(Invoicecontenttable record);

    /**
     * 根据主键ID查询并返回invoicecontenttable表中的一条记录
     *
     * @param id 主键ID
     * @return 查询到的记录
     */
    Invoicecontenttable selectByPrimaryKey(Integer id);

    /**
     * 根据主键更新invoicecontenttable表中的记录，只有非空字段会被更新
     *
     * @param record 要更新的记录
     * @return 更新的记录数
     */
    int updateByPrimaryKeySelective(Invoicecontenttable record);

    /**
     * 根据主键完全更新invoicecontenttable表中的记录，所有字段都会被更新
     *
     * @param record 要更新的记录
     * @return 更新的记录数
     */
    int updateByPrimaryKey(Invoicecontenttable record);
}