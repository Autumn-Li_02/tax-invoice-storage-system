package cn.li.projectli.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author 秋殇
 * @Date 2024/1/31 16:04
 * @Version 1.0
 */
@Configuration
@MapperScan("mappers")
public class RPAConfig {
}
