package cn.li.projectli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectLiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjectLiApplication.class, args);
    }

}
